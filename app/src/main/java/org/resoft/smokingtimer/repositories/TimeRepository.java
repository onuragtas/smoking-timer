package org.resoft.smokingtimer.repositories;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import org.resoft.smokingtimer.entities.Time;

@Dao
public interface TimeRepository {

    @Query("SELECT * FROM times WHERE `from` = 'start'")
    Time getStart();

    @Query("SELECT * FROM times WHERE `from` = 'finish'")
    Time getFinish();

    @Query("UPDATE times SET date = :time WHERE `from` = 'start'")
    void updateStart(Long time);

    @Query("UPDATE times SET date = :time WHERE `from` = 'finish'")
    void updateFinish(Long time);

    @Insert
    void add(Time time);

    @Query("DELETE FROM times WHERE `from` = 'finish'")
    void deleteFinish();

    @Query("DELETE FROM times WHERE `from` = 'start'")
    void deleteStart();

}
