package org.resoft.smokingtimer.services;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.Nullable;

import com.dinuscxj.progressbar.CircleProgressBar;

import org.resoft.smokingtimer.Database;
import org.resoft.smokingtimer.entities.Time;
import org.resoft.smokingtimer.receivers.AlarmReceiver;
import org.resoft.smokingtimer.utils.Alarm;

public class AlarmService extends IntentService {
    public AlarmService() {
        super("AlarmService");
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Alarm.triggerAlarm(getApplicationContext());
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
    }

    public int remainingSeconds(Database db){
        Time start = db.times().getStart();
        Time finish = db.times().getFinish();

        int max = finish.getDate().intValue()/1000;
        int min = start.getDate().intValue()/1000;

        int now = ((int) (System.currentTimeMillis())/1000);

        int fark = max - min;

        int alarm = fark - now % fark;

        return alarm;
    }


}
