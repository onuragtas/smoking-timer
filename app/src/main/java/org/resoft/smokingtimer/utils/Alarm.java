package org.resoft.smokingtimer.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import org.resoft.smokingtimer.Database;
import org.resoft.smokingtimer.entities.Time;
import org.resoft.smokingtimer.receivers.AlarmReceiver;

public class Alarm {

    private static PendingIntent alarmIntent;
    private static final int REQUEST_CODE = 100;

    public static void cancelAlarm(Context context) {

        Intent intent = new Intent(context, AlarmReceiver.class);
        alarmIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(alarmIntent);
    }

    public static void startAlarm(Context context, int time) {
        cancelAlarm(context);
        alarmIntent = createIntent(context);
        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmMgr.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + time, alarmIntent);
    }

    public static PendingIntent createIntent(Context context){
        Intent intent = new Intent(context, AlarmReceiver.class);
        alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        return alarmIntent;
    }


    public static void triggerAlarm(Context context){
        try {
            Database db = Database.getDatabase(context);

            Time finishNew = db.times().getFinish();
            Time startNew = db.times().getStart();

            int max = finishNew.getDate().intValue();
            int min = startNew.getDate().intValue();

            int fark = max - min;

            int now = (int) (System.currentTimeMillis());

            int alarm = fark - now % fark;

            if (alarm > 0) {
                Alarm.startAlarm(context, alarm);
            }

            Toast.makeText(context, "Alarm set to after seconds " + (fark - now % fark), Toast.LENGTH_LONG).show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
