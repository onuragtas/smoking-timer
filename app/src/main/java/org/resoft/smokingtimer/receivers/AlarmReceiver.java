package org.resoft.smokingtimer.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import org.resoft.smokingtimer.Database;
import org.resoft.smokingtimer.MainActivity;
import org.resoft.smokingtimer.entities.Time;
import org.resoft.smokingtimer.services.AlarmService;
import org.resoft.smokingtimer.utils.Alarm;
import org.resoft.smokingtimer.utils.Notification;

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        try {
            if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
                Intent serviceIntent = new Intent(context, AlarmService.class);
                context.startService(serviceIntent);
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        System.out.println("trigged" + System.currentTimeMillis());

        Alarm.triggerAlarm(context);

        Notification.create(context, "Smoke!", "", MainActivity.class);

    }
}
