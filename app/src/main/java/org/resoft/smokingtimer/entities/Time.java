package org.resoft.smokingtimer.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "times")
public class Time {

    @PrimaryKey(autoGenerate = true)
    private Long id;

    @ColumnInfo()
    private Long date;

    @ColumnInfo()
    private String from;

    public Time(Long date, String from) {
        this.date = date;
        this.from = from;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }
}
