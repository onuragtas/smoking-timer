package org.resoft.smokingtimer;

import android.content.Context;

import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import org.resoft.smokingtimer.entities.Time;
import org.resoft.smokingtimer.repositories.TimeRepository;
import org.resoft.smokingtimer.utils.Converters;

@androidx.room.Database(entities = {Time.class}, version = 3)
public abstract class Database extends RoomDatabase {

    private static Database INSTANCE;

    public abstract TimeRepository times();

    public static Database getDatabase(Context context){
        if(INSTANCE == null){
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), Database.class, "smoking_timer").allowMainThreadQueries().build();
        }
        return  INSTANCE;
    }

    public  static  void destroyInstance(){
        INSTANCE = null;
    }

}
