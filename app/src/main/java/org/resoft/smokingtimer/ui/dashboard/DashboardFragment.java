package org.resoft.smokingtimer.ui.dashboard;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.dinuscxj.progressbar.CircleProgressBar;

import org.resoft.smokingtimer.Database;
import org.resoft.smokingtimer.R;
import org.resoft.smokingtimer.entities.Time;

import java.util.Timer;
import java.util.TimerTask;

public class DashboardFragment extends Fragment {

    private DashboardViewModel dashboardViewModel;
    private Timer timer;
    private CircleProgressBar progressBar;
    private Database db;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);

        progressBar = (CircleProgressBar) root.findViewById(R.id.line_progress);

        db = Database.getDatabase(getContext());

        progressBar.setProgressFormatter(new MyProgressFormatter());

        startCounter();

        return root;
    }

    public void startCounter(){
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                setupProgress(db, progressBar);
            }
        }, 0, 1000);
    }

    private static final class MyProgressFormatter implements CircleProgressBar.ProgressFormatter {
        private static final String DEFAULT_PATTERN = "%s";

        @Override
        public CharSequence format(int progress, int max) {
            return f(progress);
        }

        public String f(int totalSecs){
            int hours = totalSecs / 3600;
            int minutes = (totalSecs % 3600) / 60;
            int seconds = totalSecs % 60;

            return String.format("%02d:%02d:%02d", hours, minutes, seconds);
        }
    }

    public void setupProgress(Database db, CircleProgressBar pg){
        Time start = db.times().getStart();
        Time finish = db.times().getFinish();

        if (start != null && finish != null) {
            int max = finish.getDate().intValue() / 1000;
            int min = start.getDate().intValue() / 1000;

            int now = ((int) (System.currentTimeMillis()) / 1000);

            int fark = max - min;

            pg.setMax(max - min);
            pg.setProgress(fark - now % fark);
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        timer.cancel();
        timer.purge();
    }

    @Override
    public void onResume() {
        super.onResume();
        startCounter();
    }
}
