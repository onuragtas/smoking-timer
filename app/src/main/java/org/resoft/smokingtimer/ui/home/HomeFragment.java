package org.resoft.smokingtimer.ui.home;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import org.resoft.smokingtimer.Database;
import org.resoft.smokingtimer.R;
import org.resoft.smokingtimer.entities.Time;
import org.resoft.smokingtimer.receivers.AlarmReceiver;
import org.resoft.smokingtimer.utils.Alarm;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

public class HomeFragment extends Fragment implements View.OnClickListener {

    private HomeViewModel homeViewModel;
    private Button startBtn, finishBtn;
    final static int REQUEST_CODE = 1;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        startBtn = root.findViewById(R.id.startBtn);
        finishBtn = root.findViewById(R.id.finishBtn);

        startBtn.setOnClickListener(this);
        finishBtn.setOnClickListener(this);

        return root;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.startBtn:
                first();
                break;
            case R.id.finishBtn:
                second();
                break;
        }
    }

    private void second() {
        Database db = Database.getDatabase(getContext());
        Time finish = db.times().getFinish();
        Time start = db.times().getStart();
        if (finish == null && start != null){
            db.times().add(new Time(System.currentTimeMillis(), "finish"));
        }else if (finish != null && start != null){
            long now = System.currentTimeMillis();

            db.times().updateFinish(now - (now - start.getDate()) % 1000);
        }

        Alarm.triggerAlarm(getContext());

    }

    private void first() {
        Database db = Database.getDatabase(getContext());
        Time start = db.times().getStart();
//        db.times().deleteFinish();
//        db.times().deleteStart();
        if (start == null){
            db.times().add(new Time(System.currentTimeMillis(),"start"));
        }else{
            db.times().updateStart(System.currentTimeMillis());
        }
    }
}
